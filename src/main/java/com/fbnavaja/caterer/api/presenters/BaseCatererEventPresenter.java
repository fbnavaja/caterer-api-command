package com.fbnavaja.caterer.api.presenters;

import com.fbnavaja.caterer.api.ports.presenters.CatererEvent;
import com.fbnavaja.caterer.api.ports.usecases.CatererEventResponse;

import java.time.format.DateTimeFormatter;

public class BaseCatererEventPresenter {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_DATE;

    protected BaseCatererEventPresenter() {}

    public static CatererEvent mapToCatererEvent(CatererEventResponse response) {
        return CatererEvent
                .builder()
                .id(response.getId())
                .catererId(response.getCatererId())
                .catererName(response.getCatererName())
                .eventDescription(response.getEventDescription())
                .createdDateTime(response.getCreatedDateTime())
                .build();
    }

}
