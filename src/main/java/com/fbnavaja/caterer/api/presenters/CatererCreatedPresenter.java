package com.fbnavaja.caterer.api.presenters;

import com.fbnavaja.caterer.api.ports.presenters.Caterer;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreated;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreatedOutput;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.NewCatererResponse;

public class CatererCreatedPresenter implements CatererCreatedOutput {
    private CatererCreated newCaterer;
    public CatererCreated getNewCaterer() {
        return newCaterer;
    }

    @Override
    public void present(NewCatererResponse newCatererResponse) {
        newCaterer = new CatererCreated(newCatererResponse.getId().toString());
    }
}
