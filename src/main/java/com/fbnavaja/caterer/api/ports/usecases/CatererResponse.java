package com.fbnavaja.caterer.api.ports.usecases;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CatererResponse {
    private String id;
    private String name;
    private String city;
    private String street;
    private int zip;
    private int min_guests;
    private int max_guests;
    private String phoneNumber;
    private String mobileNumber;
    private String emailAddress;
}
