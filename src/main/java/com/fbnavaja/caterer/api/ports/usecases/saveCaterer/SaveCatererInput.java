package com.fbnavaja.caterer.api.ports.usecases.saveCaterer;

import com.fbnavaja.caterer.api.domain.entities.Caterer;

public interface SaveCatererInput {
    void exec(CatererRequest request);

    default Caterer.ContactInfo buildContactInfo(CatererRequest request) {
        return Caterer.ContactInfo.builder().mobileNumber(request.getMobileNumber()).phoneNumber(request.getPhoneNumber()).emailAddress(request.getEmailAddress()).build();
    }

    default Caterer.Capacity buildCapacity(CatererRequest request) {
        return Caterer.Capacity.builder().min_guests(request.getMin_guests()).max_guests(request.getMax_guests()).build();
    }

    default Caterer.Location buildLocation(CatererRequest request) {
        return Caterer.Location.builder().city(request.getCity()).street(request.getStreet()).zip(request.getZip()).build();
    }
}
