package com.fbnavaja.caterer.api.ports.usecases.deleteCaterer;

public interface DeleteCatererInput {
    void exec(DeleteCatererRequest request);
}
