package com.fbnavaja.caterer.api.ports.presenters;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class Caterer {

    private String id;
    private String name;
    private Location location;
    private Capacity capacity;
    private ContactInfo contactInfo;

    @Builder
    @Getter
    public static class Location {
        private String city;
        private String street;
        private int zip;
    }

    @Builder
    @Getter
    public static class Capacity {
        private int min_guests;
        private int max_guests;
    }

    @Builder
    @Getter
    public static class ContactInfo {
        private String phoneNumber;
        private String mobileNumber;
        private String emailAddress;
    }

}
