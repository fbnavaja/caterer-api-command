package com.fbnavaja.caterer.api.ports.presenters;

import com.fbnavaja.caterer.api.ports.usecases.CatererResponse;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.NewCatererResponse;

public interface CatererCreatedOutput {
    CatererCreated getNewCaterer();
    void present(NewCatererResponse newCatererResponse);
}
