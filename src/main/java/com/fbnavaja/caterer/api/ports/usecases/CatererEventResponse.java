package com.fbnavaja.caterer.api.ports.usecases;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CatererEventResponse {
    private String id;
    private String catererId;
    private String catererName;
    private String eventDescription;
    private LocalDateTime createdDateTime;
}
