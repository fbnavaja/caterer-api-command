package com.fbnavaja.caterer.api.ports.database;

public interface CatererDatabase {
    CatererGateway catererGateway();
}
