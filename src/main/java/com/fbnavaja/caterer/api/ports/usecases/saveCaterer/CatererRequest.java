package com.fbnavaja.caterer.api.ports.usecases.saveCaterer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CatererRequest {
    private String id;
    @NotEmpty(message = "The name is required.")
    @Pattern(regexp = "^[A-Za-z]+$", message = "Caterer name must not contain digits and special characters")
    private String name;
    private String city;
    private String street;
    private int zip;
    private int min_guests;
    private int max_guests;
    private String phoneNumber;
    private String mobileNumber;
    private String emailAddress;
}
