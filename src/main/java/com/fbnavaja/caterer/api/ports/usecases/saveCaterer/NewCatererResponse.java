package com.fbnavaja.caterer.api.ports.usecases.saveCaterer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class NewCatererResponse {
    private String id;
}
