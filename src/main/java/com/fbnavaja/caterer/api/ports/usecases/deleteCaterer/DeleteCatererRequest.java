package com.fbnavaja.caterer.api.ports.usecases.deleteCaterer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeleteCatererRequest {
    private String id;
}
