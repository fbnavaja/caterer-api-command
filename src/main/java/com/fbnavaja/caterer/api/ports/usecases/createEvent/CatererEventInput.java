package com.fbnavaja.caterer.api.ports.usecases.createEvent;

import com.fbnavaja.caterer.api.domain.entities.Caterer;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CatererRequest;

public interface CatererEventInput {
    void exec(CatererEventRequest request);
}
