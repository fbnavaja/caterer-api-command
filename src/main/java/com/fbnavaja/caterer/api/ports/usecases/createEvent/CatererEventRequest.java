package com.fbnavaja.caterer.api.ports.usecases.createEvent;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class CatererEventRequest {
    private String id;
    private String catererId;
    private String catererName;
    private String eventDescription;
    private LocalDateTime createdDateTime;


}
