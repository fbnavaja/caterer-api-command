package com.fbnavaja.caterer.api.ports.presenters;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CatererCreated {
    private String id;
}
