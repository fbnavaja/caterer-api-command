package com.fbnavaja.caterer.api.application.commands;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.*;

@Data
@ApiModel(value = "New Caterer Request", subTypes = {ClientCatererRequest.Location.class, ClientCatererRequest.Capacity.class, ClientCatererRequest.ContactInfo.class})
public class ClientCatererRequest {
    @NotEmpty(message = "The Caterer name is required.")
    private String name;
    @Valid
    private ClientCatererRequest.Location location;
    @Valid
    private ClientCatererRequest.Capacity capacity;
    @Valid
    private ClientCatererRequest.ContactInfo contactInfo;

    @Builder
    @Data
    @ApiModel(value = "Caterer Location", parent = ClientCatererRequest.class)
    public static class Location {
        @NotEmpty(message = "City name is required.")
        @Pattern(regexp = "[ a-z A-Z ]+", message = "City name must not contain digits and special characters")
        private String city;
        @NotEmpty(message = "Street is required.")
        @NotEmpty(message = "The Street is required.")
        private String street;
        private int zip;
    }

    @Builder
    @Data
    @ApiModel(value = "Caterer Capacity", parent = ClientCatererRequest.class)
    public static class Capacity {
        @Positive(message = "Min guests must not be blank and contains only positive integer value")
        private int min_guests;
        @Positive(message = "Max guests must not be blank and contains only positive integer value")
        private int max_guests;
    }

    @Builder
    @Data
    @ApiModel(value = "Caterer Contact Information", parent = ClientCatererRequest.class)
    public static class ContactInfo {
        private String phoneNumber;
        @NotEmpty(message = "Mobile number is required")
        private String mobileNumber;
        @NotEmpty(message = "Email address is required")
        @Email(message = "Email address must be a valid format")
        private String emailAddress;
    }

}
