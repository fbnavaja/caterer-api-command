package com.fbnavaja.caterer.api.application.config;

import com.fbnavaja.caterer.api.database.jpa.JpaCatererDatabase;
import com.fbnavaja.caterer.api.database.jpa.JpaCatererEventDatabase;
import com.fbnavaja.caterer.api.database.jpa.repositories.CatererEventRepository;
import com.fbnavaja.caterer.api.database.jpa.repositories.CatererRepository;
import com.fbnavaja.caterer.api.infrastructure.eventsourcing.KafkaCatererEventSource;
import com.fbnavaja.caterer.api.ports.database.CatererDatabase;
import com.fbnavaja.caterer.api.ports.database.CatererEventDatabase;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreatedOutput;
import com.fbnavaja.caterer.api.ports.presenters.CatererOutput;
import com.fbnavaja.caterer.api.ports.usecases.createEvent.CatererEventInput;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CreateCatererInput;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.SaveCatererInput;
import com.fbnavaja.caterer.api.presenters.CatererCreatedPresenter;
import com.fbnavaja.caterer.api.presenters.CatererPresenter;
import com.fbnavaja.caterer.api.usecases.createCaterer.CreateCaterer;
import com.fbnavaja.caterer.api.usecases.createEvent.CreateCatererEvent;
import com.fbnavaja.caterer.api.usecases.saveCaterer.SaveCaterer;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EntityScan("com.fbnavaja.caterer.api.database.jpa.data")
@EnableMongoRepositories("com.fbnavaja.caterer.api.database.jpa.repositories")
public class CatererAppConfiguration {

    @Bean
    public CatererDatabase catererDatabase(CatererRepository catererRepository) {
        return new JpaCatererDatabase(catererRepository);
    }

    @Bean
    public CatererEventDatabase catererEventDatabase(CatererEventRepository catererEventRepository) {
        return new JpaCatererEventDatabase(catererEventRepository);
    }

    @Bean
    public KafkaCatererEventSource messageBroker() {
        return new KafkaCatererEventSource();
    }


    @Bean
    public CatererOutput catererOutput() {

        return new CatererPresenter();
    }

    @Bean
    public CreateCatererInput createCatererInput(CatererCreatedOutput catererCreatedOutput, CatererDatabase catererDatabase) {
        return new CreateCaterer(catererCreatedOutput, catererDatabase.catererGateway(), messageBroker());
    }

    @Bean
    public SaveCatererInput saveCatererInput(CatererDatabase catererDatabase) {
        return new SaveCaterer(catererDatabase.catererGateway(), messageBroker());
    }

    @Bean
    public CatererEventInput createCatererEventInput(CatererEventDatabase catererEventDatabase) {
        return new CreateCatererEvent(catererEventDatabase.catererEventGateway());
    }

    @Bean
    public CatererCreatedOutput catererCreatedOutput() {
        return new CatererCreatedPresenter();
    }

}
