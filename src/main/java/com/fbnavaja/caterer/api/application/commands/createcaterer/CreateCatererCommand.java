package com.fbnavaja.caterer.api.application.commands.createcaterer;

import com.fbnavaja.caterer.api.application.commands.BaseCommand;
import com.fbnavaja.caterer.api.application.commands.ClientCatererRequest;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreated;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreatedOutput;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CreateCatererInput;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.text.MessageFormat;

@RestController
public class CreateCatererCommand implements BaseCommand {
    private final CreateCatererInput useCase;
    private final CatererCreatedOutput presenter;


    public CreateCatererCommand(CreateCatererInput useCase, CatererCreatedOutput presenter) {
        this.useCase = useCase;
        this.presenter = presenter;
    }

    @PostMapping("/caterers")
    @ApiOperation(value = "Create Caterer", response = CatererCreated.class)
    public ResponseEntity exec(@Valid @RequestBody ClientCatererRequest request) {
        useCase.exec(
                com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CatererRequest.builder()
                        .name(request.getName())
                        .city(request.getLocation().getCity())
                        .street(request.getLocation().getStreet())
                        .zip(request.getLocation().getZip())
                        .min_guests(request.getCapacity().getMin_guests())
                        .max_guests(request.getCapacity().getMax_guests())
                        .mobileNumber(request.getContactInfo().getMobileNumber())
                        .phoneNumber(request.getContactInfo().getPhoneNumber())
                        .emailAddress(request.getContactInfo().getEmailAddress())
                        .build()
        );

        return ResponseEntity
                .created(
                        URI.create(MessageFormat.format("/api/v1/caterers/{0}", presenter.getNewCaterer().getId()))
                )
                .body(presenter.getNewCaterer());
    }


}
