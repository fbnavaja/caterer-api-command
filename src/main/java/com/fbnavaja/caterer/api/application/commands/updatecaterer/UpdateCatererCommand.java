package com.fbnavaja.caterer.api.application.commands.updatecaterer;

import com.fbnavaja.caterer.api.application.commands.BaseCommand;
import com.fbnavaja.caterer.api.application.commands.ClientCatererRequest;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreated;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CatererRequest;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.SaveCatererInput;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class UpdateCatererCommand implements BaseCommand {
    private final SaveCatererInput useCase;


    public UpdateCatererCommand(SaveCatererInput useCase) {
        this.useCase = useCase;
    }

    @PutMapping("/caterers/{id}")
    @ApiOperation(value = "Create Caterer", response = CatererCreated.class)
    public ResponseEntity exec(@Valid @RequestBody ClientCatererRequest request, @PathVariable String id) {
        useCase.exec(
                CatererRequest.builder()
                        .id(id)
                        .name(request.getName())
                        .city(request.getLocation().getCity())
                        .street(request.getLocation().getStreet())
                        .zip(request.getLocation().getZip())
                        .min_guests(request.getCapacity().getMin_guests())
                        .max_guests(request.getCapacity().getMax_guests())
                        .mobileNumber(request.getContactInfo().getMobileNumber())
                        .phoneNumber(request.getContactInfo().getPhoneNumber())
                        .emailAddress(request.getContactInfo().getEmailAddress())
                        .build()
        );

         return ResponseEntity
                .ok().build();
    }


}
