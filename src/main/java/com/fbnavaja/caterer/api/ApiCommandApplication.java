package com.fbnavaja.caterer.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCommandApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCommandApplication.class, args);
	}

}
