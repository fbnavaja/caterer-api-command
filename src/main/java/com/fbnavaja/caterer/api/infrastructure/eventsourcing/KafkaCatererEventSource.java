package com.fbnavaja.caterer.api.infrastructure.eventsourcing;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fbnavaja.caterer.api.infrastructure.eventsourcing.events.CatererCreatedEvent;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
public class KafkaCatererEventSource {

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    @Value(value="${message.topic.catererApi}")
    private String topic;

    public void catererCreatedEvent(String id, String name) throws JsonProcessingException {
        CatererCreatedEvent catererCreatedEvent =
                CatererCreatedEvent.builder()
                        .catererId(id)
                        .catererName(name)
                        .createdDateTime(LocalDateTime.now()).build();
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        val json = objectWriter.writeValueAsString(catererCreatedEvent);

        kafkaTemplate.send(topic, json);
    }

}
