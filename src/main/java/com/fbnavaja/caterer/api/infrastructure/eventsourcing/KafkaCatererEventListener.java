package com.fbnavaja.caterer.api.infrastructure.eventsourcing;

import com.fbnavaja.caterer.api.infrastructure.eventsourcing.events.CatererCreatedEvent;
import com.fbnavaja.caterer.api.ports.usecases.createEvent.CatererEventInput;
import com.fbnavaja.caterer.api.ports.usecases.createEvent.CatererEventRequest;
import com.google.gson.Gson;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class KafkaCatererEventListener {

    private final CatererEventInput useCase;
    private final CountDownLatch latch = new CountDownLatch(3);

    public KafkaCatererEventListener(CatererEventInput useCase) {
        this.useCase = useCase;
    }


    @KafkaListener(topics="${message.topic.catererApi}", groupId = "${spring.kafka.consumer.group-id}")
    public void listenToEvent(ConsumerRecord<String, String> consumer) throws Exception {
        CatererCreatedEvent catererCreatedEvent = new Gson().fromJson(consumer.value(), CatererCreatedEvent.class);
        CatererEventRequest catererEventRequest = CatererEventRequest.builder()
                            .catererId(catererCreatedEvent.getCatererId())
                            .catererName(catererCreatedEvent.getCatererName())
                            .eventDescription("New Caterer created: " + catererCreatedEvent.getCatererName())
                            .createdDateTime(catererCreatedEvent.getCreatedDateTime())
                            .build();
        useCase.exec(catererEventRequest);
        latch.countDown();
    }
}
