package com.fbnavaja.caterer.api.domain.entities;

import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@ToString
@EqualsAndHashCode
public class CatererEvent {
    private String id;
    private String catererId;
    private String catererName;
    private String eventDescription;
    private LocalDateTime createdDateTime;
}
