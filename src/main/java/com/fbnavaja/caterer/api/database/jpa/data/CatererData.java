package com.fbnavaja.caterer.api.database.jpa.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class CatererData {
    @Id
    private String id;
    private String name;
    private String city;
    private String street;
    private int zip;
    private int min_guests;
    private int max_guests;
    private String phoneNumber;
    private String mobileNumber;
    private String emailAddress;
}
