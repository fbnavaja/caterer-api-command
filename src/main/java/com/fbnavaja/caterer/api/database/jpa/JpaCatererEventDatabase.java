package com.fbnavaja.caterer.api.database.jpa;

import com.fbnavaja.caterer.api.database.jpa.repositories.CatererEventRepository;
import com.fbnavaja.caterer.api.database.jpa.repositories.CatererRepository;
import com.fbnavaja.caterer.api.ports.database.CatererDatabase;
import com.fbnavaja.caterer.api.ports.database.CatererEventDatabase;
import com.fbnavaja.caterer.api.ports.database.CatererEventGateway;
import com.fbnavaja.caterer.api.ports.database.CatererGateway;

public class JpaCatererEventDatabase implements CatererEventDatabase {
    private final CatererEventGateway catererEventGateway;

    public JpaCatererEventDatabase(CatererEventRepository catererEventRepository) {
        this.catererEventGateway = new JpaCatererEventGateway(catererEventRepository);
    }

    @Override
    public CatererEventGateway catererEventGateway() {

        return catererEventGateway;
    }
}
