package com.fbnavaja.caterer.api.database.jpa;

import com.fbnavaja.caterer.api.database.jpa.repositories.CatererRepository;
import com.fbnavaja.caterer.api.ports.database.CatererGateway;
import com.fbnavaja.caterer.api.ports.database.CatererDatabase;

public class JpaCatererDatabase implements CatererDatabase {
    private final CatererGateway catererGateway;

    public JpaCatererDatabase(CatererRepository catererRepository) {
        this.catererGateway = new JpaCatererGateway(catererRepository);
    }

    @Override
    public CatererGateway catererGateway() {
        return catererGateway;
    }
}
