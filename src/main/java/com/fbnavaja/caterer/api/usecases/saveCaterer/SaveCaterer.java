package com.fbnavaja.caterer.api.usecases.saveCaterer;

import com.fbnavaja.caterer.api.domain.entities.Caterer;
import com.fbnavaja.caterer.api.infrastructure.eventsourcing.KafkaCatererEventSource;
import com.fbnavaja.caterer.api.ports.database.CatererGateway;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreatedOutput;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CatererRequest;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.NewCatererResponse;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.SaveCatererInput;

import java.util.UUID;

public class SaveCaterer implements SaveCatererInput {
    private final CatererGateway catererGateway;

    public SaveCaterer(CatererGateway catererGateway, KafkaCatererEventSource kafkaCatererEventSource) {
        this.catererGateway = catererGateway;
    }

    @Override
    public void exec(CatererRequest request) {
        saveCaterer(request);
    }

    private void saveCaterer(CatererRequest request) {
       catererGateway.saveCaterer(
            Caterer.builder()
                    .id(request.getId())
                    .name(request.getName())
                    .location(buildLocation(request))
                    .capacity(buildCapacity(request))
                    .contactInfo(buildContactInfo(request))
                    .build()

        );
    }


}
