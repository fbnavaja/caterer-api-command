package com.fbnavaja.caterer.api.usecases.createCaterer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fbnavaja.caterer.api.domain.entities.Caterer;
import com.fbnavaja.caterer.api.infrastructure.eventsourcing.KafkaCatererEventSource;
import com.fbnavaja.caterer.api.ports.database.CatererGateway;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreatedOutput;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CatererRequest;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CreateCatererInput;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.NewCatererResponse;

public class CreateCaterer implements CreateCatererInput {
    private final CatererCreatedOutput presenter;
    private final CatererGateway catererGateway;
    private final KafkaCatererEventSource eventSourcing;

    public CreateCaterer(CatererCreatedOutput presenter, CatererGateway catererGateway, KafkaCatererEventSource eventSourcing) {
        this.presenter = presenter;
        this.catererGateway = catererGateway;
        this.eventSourcing = eventSourcing;
    }

    @Override
    public void exec(CatererRequest request) {
        String id = createCaterer(request);

        NewCatererResponse newCatererResponse = new NewCatererResponse(id);
        presenter.present(newCatererResponse);
    }

    private String createCaterer(CatererRequest request) {
        String newId =  catererGateway.createCaterer(
            Caterer.builder()
                    .name(request.getName())
                    .location(buildLocation(request))
                    .capacity(buildCapacity(request))
                    .contactInfo(buildContactInfo(request))
                    .build()

        );
        try {
            eventSourcing.catererCreatedEvent(newId, request.getName());
        } catch (JsonProcessingException e) {
            //failed to process. silent for now.
        }

        return newId;

    }
}
