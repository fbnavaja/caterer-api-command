package com.fbnavaja.caterer.api.usecases.createEvent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fbnavaja.caterer.api.application.commands.BaseCommand;
import com.fbnavaja.caterer.api.domain.entities.Caterer;
import com.fbnavaja.caterer.api.domain.entities.CatererEvent;
import com.fbnavaja.caterer.api.infrastructure.eventsourcing.KafkaCatererEventSource;
import com.fbnavaja.caterer.api.ports.database.CatererEventGateway;
import com.fbnavaja.caterer.api.ports.database.CatererGateway;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreatedOutput;
import com.fbnavaja.caterer.api.ports.usecases.createEvent.CatererEventInput;
import com.fbnavaja.caterer.api.ports.usecases.createEvent.CatererEventRequest;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CatererRequest;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CreateCatererInput;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.NewCatererResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

public class CreateCatererEvent implements CatererEventInput {
    private final CatererEventGateway catererEventGateway;


    public CreateCatererEvent(CatererEventGateway catererEventGateway) {
        this.catererEventGateway = catererEventGateway;
    }

    public void exec(CatererEventRequest request) {

        createCatererEvent(request);
    }

    private void createCatererEvent(CatererEventRequest request) {
        catererEventGateway.createCatererEvent(
                CatererEvent.builder()
                        .id(request.getId())
                        .catererId(request.getCatererId())
                        .catererName(request.getCatererName())
                        .eventDescription(request.getEventDescription())
                        .createdDateTime(request.getCreatedDateTime())
                        .build()

        );

    }

}
