package com.fbnavaja.caterer.api.usecases.deleteCaterer;

import com.fbnavaja.caterer.api.ports.database.CatererGateway;
import com.fbnavaja.caterer.api.ports.presenters.CatererCreatedOutput;
import com.fbnavaja.caterer.api.ports.usecases.deleteCaterer.DeleteCatererInput;
import com.fbnavaja.caterer.api.ports.usecases.deleteCaterer.DeleteCatererRequest;

public class DeleteCaterer implements DeleteCatererInput {
    private final CatererGateway catererGateway;

    public DeleteCaterer(CatererGateway catererGateway) {
        this.catererGateway = catererGateway;
    }

    @Override
    public void exec(DeleteCatererRequest request) {

        catererGateway.deleteCaterer(request.getId());
    }
}
