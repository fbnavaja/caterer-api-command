package com.fbnavaja.caterer.api.usecases.saveCaterer;

import com.fbnavaja.caterer.api.domain.entities.Caterer;
import com.fbnavaja.caterer.api.ports.database.CatererGateway;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CatererRequest;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.CreateCatererInput;
import com.fbnavaja.caterer.api.ports.usecases.saveCaterer.SaveCatererInput;

public class CreateCaterer implements CreateCatererInput {
    private final CatererGateway catererGateway;

    public CreateCaterer(CatererGateway catererGateway) {
        this.catererGateway = catererGateway;
    }

    @Override
    public void exec(CatererRequest request) {
        saveCaterer(request);
    }

    private void saveCaterer(CatererRequest request) {
       catererGateway.saveCaterer(
            Caterer.builder()
                    .name(request.getName())
                    .location(buildLocation(request))
                    .capacity(buildCapacity(request))
                    .contactInfo(buildContactInfo(request))
                    .build()

        );
    }


}
